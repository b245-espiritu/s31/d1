const http = require('http');

const port = 4000;

const server = http.createServer( (request, response) => {

    // Accessing the "greeting " route returns a message of "Hello World"
    if(request.url == '/greeting'){
        response.writeHead(200, {'Content-type': 'text/plain'});
        response.end('Hello World');

        
    }
    else if(request.url == '/homepage'){
        response.writeHead(200, {'Content-type': 'text/plain'});
        response.end('Welcome to our Homepage');
    }

    else{
        // Set a status code for the response - a 404 means "NOT FOUND"
        response.writeHead(404, {'Content-type': 'text/plain'});
        response.end('Page not Available')
    }
}).listen(port);

console.log(`Server now accessible at localhost: ${port}`);
